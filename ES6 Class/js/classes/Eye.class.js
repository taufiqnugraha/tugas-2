class Eye extends Circle {
    
    constructor(id, x, y, radius, color) {
        super(id, x, y, radius);
        this.color = color;
    }

    roll (rollAmount) {
        this.move(this.x,this.y);
        let result = rollAmount + this.x;
        return (this.x +' + '+ rollAmount + " = " + result );
        // return ('x :'+ x  + '+' + ' rollAmount: '+ rollAmount +" ="+ "result" +result);
    }   
}
